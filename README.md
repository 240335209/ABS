# ABS

---

The Axelor Business Suite based on Axelor Development Kit

Axelor Business Suite 是法国公司Axelor (https://www.axelor.com) 开发的一套ERP系统。

ABS是 Axelor Business Suite的缩写。

本项目是基于Axelor Business Suite 4.1.0 / Axelor Development Kit 4.1.8 的代码进行本地化开发：

1、简体中文版

2、二次开发技术支持

ABS由PSI技术团队提供技术支持，关于PSI更多信息请访问：https://gitee.com/crm8000/PSI

# QQ群

---


群号：581209519


# WIKI

---

https://gitee.com/crm8000/ABS/wikis

# 技术文档

---

https://gitee.com/mjc123/ABS_TechDoc